# Blender: 3D scene, modeling, animation and rendering

In this repository, you can find my computergraphics project. A documentation is provided as .pdf and as .tex file. 
There is a folder for each task including the .blend files and the rendered images and animations as well. Furthermore, you can find the used resources (e.g. downloaded models in task 1 and 2). 
Most of the rendered images are included in the documentation, too. For some tasks, I provided additional images.
The animation movies are not integrated in the documentation. 
